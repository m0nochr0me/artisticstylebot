# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import queue
import threading
import time
import subprocess
import telebot
import requests
import shutil
from time import time, sleep
import random
import urllib
import logging

# logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s', filename='bot.log',level=logging.INFO, datefmt='%m/%d/%Y %I:%M:%S %p')

logger = logging.getLogger("ArtisticStyleBot")
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler('ArtisticStyleBot.log')
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname)s:%(message)s')
formatter.datefmt = '%d/%m/%Y %H:%M:%S'
fh.setFormatter(formatter)
logger.addHandler(fh)

styles = {}

API_TOKEN = '226904723:AAEiBQfl4ZV_1V173wbErUrBucb_b7hXOw0'

bot = telebot.TeleBot(API_TOKEN)

q = queue.Queue()


@bot.message_handler(commands=['start'])
def send_welcome(message):
    chat_id = message.chat.id
    logger.info(
        '{0} -- {1} started to use bot'.format(message.chat.username, chat_id))
    info = (
        'Send one or more photos to this bot and wait ;)\n'
        'Every time bot will use another random style,'
        ' unless you override it with /style command.\n'
        'To see all available styles use /guide command.\n\n'
        '---\n\n'
        'Отправьте боту одну или несколько фотографий и немного подождите ;)\n'
        'Каждый раз бот будет использовать случайный стиль, если только вы не'
        ' переопределите его командой /style.\n'
        'Чтобы получить список доступных стилей отправьте комадну /guide.\n\n'
        '---\n\n'
        'Original document: http://arxiv.org/abs/1508.06576\n'
        'Telegram: @m0nochr0me\n'
        'Email: m0nochr0mex@gmail.com'
    )
    bot.send_message(chat_id, info)


@bot.message_handler(commands=['help'])
def send_help(message):
    chat_id = message.chat.id
    username = message.chat.username
    logger.info('{0} -- {1} requires help'.format(username, chat_id))
    info = (
        'You can send one or more images, that will be queued'
        ' and then processed.\n'
        'Approximate processing time ~8 minutes per image.\n'
        'Available commands:\n'
        '/guide - display styles guide.\n'
        '/style - set style according to guide\n'
        '/info - display bot info'
    )
    bot.send_message(chat_id, info)


@bot.message_handler(commands=['info'])
def send_help(message):
    chat_id = message.chat.id
    username = message.chat.username
    logger.info('{0} -- {1} requires info'.format(username, chat_id))
    info = (
        'Original document: http://arxiv.org/abs/1508.06576\n\n'
        'Python, neural-style, torch\n\n'
        'Telegram: @m0nochr0me\n'
        'Email: m0nochr0mex@gmail.com\n'
        'Source code: soon\n'
        'Site: http://zero.systems'
    )
    bot.send_message(chat_id, info)


@bot.message_handler(commands=['privacy'])
def send_privacy(message):
    chat_id = message.chat.id
    username = message.chat.username
    logger.info('{0} -- {1} reads privacy info'.format(username, chat_id))
    info = ('*Here be privacy notes*\n')
    bot.send_message(chat_id, info)


@bot.message_handler(commands=['guide'])
def send_guide(message):
    logger.info('{0} got guide'.format(message.chat.username))
    chat_id = message.chat.id
    for i in range(4):
        path_out = 'preview/c{0}.png'.format(i)
        out_file = open(path_out, 'rb')
        bot.send_photo(chat_id, out_file)
    info = (
        'Pick number and send it using /style command.\n'
        '---\n'
        'Выберите номер и отправьте его после команды /style'
    )
    bot.send_message(chat_id, info)


@bot.message_handler(commands=['style'])
def send_style_init(message):
    logger.info('{0} about to set style'.format(message.chat.username))
    chat_id = message.chat.id
    info = (
        'Send number from 0 to 31 corresponding to desired style.\n'
        'To return to random send any letter.\n'
        'To learn more about send /guide command.\n'
        '---\n'
        'Отправьте число от 0 до 31 соответствующее желаемому стилю.\n'
        'Чтобы вернуться к случайному выбору отправьте любую букву.\n'
        'Чтобы получить список стилей отправьте комадну /guide'
    )
    msg = bot.send_message(chat_id, info)
    bot.register_next_step_handler(msg, process_style)


def process_style(message):
    try:
        chat_id = message.chat.id
        style = message.text
        if int(style) in range(32):
            styles[chat_id] = int(style)
            bot.send_message(
                chat_id, "Success. Style set to {0}".format(style))
            logger.info(
                '{0} set style to {1}'.format(message.chat.username, style))
        else:
            bot.send_message(
                chat_id, "Unable to set style. Please use command /style '\
                ' again and use numbers from 0 to 31.")
    except:
        bot.send_message(chat_id, "Reverting to random style choosing.")
        logger.info('{0} resets style to random'.format(message.chat.username))
        try:
            styles.pop(chat_id)
        except KeyError:
            pass


@bot.message_handler(content_types=['text'])
def handle_plaintext(message):
    username = message.chat.username
    text = message.text
    logger.info('{0} says: "{1}"'.format(username, text))


@bot.message_handler(content_types=['photo'])
def handle_photo(message):
    chat_id = message.chat.id
    chat_type = message.chat.type
    username = message.chat.username
    bot.send_message(chat_id, 'Receiving image...')
    photo = message.photo[-1]
    file_id = photo.file_id
    file_info = bot.get_file(file_id)
    file = requests.get(
        'https://api.telegram.org/file/bot{0}/{1}'.format(
                                  API_TOKEN, file_info.file_path), stream=True)
    if file.status_code == 200:
        # path = 'in/' + str(chat_id)+'_'+str(file_id)+'.jpg'
        path = 'in/{0}_{1}_{2}.jpg'.format(chat_id, file_id, time())
        with open(path, 'wb') as f:
            file.raw.decode_content = True
            shutil.copyfileobj(file.raw, f)
        q.put([chat_id, file_id, path, username])
        qpos = q.qsize()
        logger.info(
            '{0}[{3}] queued image. Pos. {1} -\n- {2}'.format(
                    username, qpos, file_id, chat_type))
        bot.send_message(
            chat_id, 'Image queued\nETA: {0} minutes.'.format(qpos*8))
        info = (
            'This bot is at early stage of development.\n'
            'Please be patient.\n'
            'If you did not received your photo in time, just try again 0:)\n'
            '---\n'
            'Бот находится на раннем этапе разработки.\n'
            'Если вы не получили свое фото вовремя, попытайтесь еще раз 0:)'
        )
        bot.send_message(chat_id, info)
    else:
        bot.send_message(chat_id, 'Something wrong!')
        logger.error('File Unavailable')
    # bot.send_message(chat_id, 'Done!')


def worker():
    while True:
        item = q.get(block=True)
        chat_id = item[0]
        file_id = item[1]
        path_in = item[2]
        username = item[3]
        logger.info(
            'Processing image from {0} -\n- {1}'.format(username, file_id))
        # path_out = 'out/'+str(chat_id)+'_'+str(file_id)+'.png'
        path_out = 'out/{0}_{1}_{2}.png'.format(chat_id, file_id, time())
        if chat_id in styles:
            style_num = styles[chat_id]
        else:
            style_num = random.randint(0, 31)
            bot.send_message(
                chat_id, "You can choose desired style with '\
                ' /guide and /style commands.")
        style = 'artists1/painting-{0}.jpg'.format(style_num)
        bot.send_message(
            chat_id, 'Processing your image now, using style {0}.\n'\
            'Please wait...'.format(style_num))
        subprocess.call(
            ["/home/m0no/src/neural-style/runrnd.sh", path_in, path_out, style])
        logger.debug('Process finished')
        out_file = open(path_out, 'rb')
        bot.send_photo(chat_id, out_file)
        logger.debug('Photo sent')
        q.task_done()

t = threading.Thread(target=worker)
t.daemon = True
t.start()

try:
    bot.polling(none_stop=True, timeout=45)
    logger.info('Polling Start')
except urllib.error.HTTPError:
    logger.exception('URLlib Error')
    sleep(15)

while True:
    sleep(20)

q.join()

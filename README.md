# README #

Artistic Style bot for Telegram

### Requirements ###

* neural-style (https://github.com/jcjohnson/neural-style)
* pyTelegramBotAPI (https://github.com/eternnoir/pyTelegramBotAPI)

### How do I get set up? ###

* Install neural-style with all prerequisites
* Install pyTelegramBotAPI
* Place ArtisticStyleBot.py and runvgg.sh to neural-style directory
* Get Telegram API key

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
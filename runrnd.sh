#!/bin/bash

CW=5
SW=25
TV="0.000001"
ITER=400
SAVEITER=0
SIZE=640
SEED=$(shuf -i 128-256000 -n 1)

#DIR='artists1'

#STYLE=$DIR/$(ls -1 $DIR | shuf -n3 | shuf -n1)
CONTENT=$1
OUT=$2
STYLE=$3

echo $STYLE

th neural_style.lua \
-style_image $STYLE \
-content_image $CONTENT \
-tv_weight $TV \
-output_image $OUT \
-model_file models/VGG_ILSVRC_19_layers.caffemodel \
-proto_file models/VGG_ILSVRC_19_layers_deploy.prototxt \
-gpu 0 \
-backend cudnn \
-normalize_gradients \
-num_iterations $ITER \
-save_iter $SAVEITER \
-seed $SEED \
-content_weight $CW \
-style_weight $SW \
-image_size $SIZE \
-optimizer adam
